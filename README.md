/* Made for Robotics Course
 *Valeria Ramirez
 *  Program: Sensor Assignment
 *  Function: Values are read from sensor and displayed on computer via serial communication. 
 *  Shows difference between white surface and black surface.
 *  Date: Nov 28, 2018
 *  Version 1
 */